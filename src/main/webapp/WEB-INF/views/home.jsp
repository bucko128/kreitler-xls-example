<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
</head>
<body>
<h2>Exporting data in XLS files</h2>
<table border=1>
<tr><th>Referent</th><th>MeaningVal</th><th>Comment</th></tr>
<c:forEach items="${rowList}" var="row">
	<tr>
		<td>${row.getText1()}</td>
		<td>${row.getText2()}</td>
		<td>${row.getText3()}</td>
	</tr>
</c:forEach>
<tr><td><br><form method="GET" action="/clear"><input type="submit" value="CLEAR TABLE" /></form></td>
<td colspan=2><br><form method="GET" action="/getxls"><input type="submit" value="download XLS file" /></form></td>
</tr>
</table>
<br>
<table border=1>
<tr><td>
<form method="POST" action="/add" accept-charset="UTF-8">
<label>Referent</label>
<input type="text"  name="text1" /><br>
<label>Meaning Value</label>
<input id="2" type="text" " name="text2" /><br>
<label>Comment</label>
<input type="text"  name="text3" />
<br>
<input type="submit" />
</td></tr>
</table>
</form>

<div ng-app="myApp" ng-controller="myCtrl"> 

<p>Today's welcome message is:</p>

<input type="button" value="send ajax" ng-click="func()" />

</div>

<p>The $http service requests a page on the server, and the response is set as the value of the "myWelcome" variable.</p>

<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
	$scope.func = function(){
		$http({
			method: "POST",
			url: "/ajaxpostadd",
			data: document.getElementById("2").value
		}).then().catch();
	}
});
</script>


<h3>Instructions</h3>
<ul>
<li>Press 'CLEAR TABLE' whenever you want to delete all existing rows.</li>
<li>Press 'download XLS file' to download all existing rows as a XLS (MS Excel) file.</li>
<li>Try using English, Hebrew and Arabic in any combination.(Hebrew referent and meaingval but Arabic for comment?)</li>
<li>!!! This page is public, whatever you put in the table is seen by all visitors !!!</li>
<li>Punctuation marks will not come out so well in the XLS file</li>
<li>Contact and notify me of any other bugs found</li>
</ul>
</body>
</html>
