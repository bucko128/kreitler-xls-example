package com.mycompany.dao;

import com.mycompany.domain.User;
import java.util.*;

public interface UserDao {
	List<User> getUserList();
    void addUser(User user);
    void updateUser(User user);
    void deleteAllUsers();
}
