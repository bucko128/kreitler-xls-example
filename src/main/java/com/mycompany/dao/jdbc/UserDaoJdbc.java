package com.mycompany.dao.jdbc;

import com.mycompany.dao.UserDao;
import com.mycompany.domain.User;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class UserDaoJdbc implements UserDao {
    private JdbcOperations jdbcOperations;

    public UserDaoJdbc(JdbcOperations jdbcOperations){
        this.jdbcOperations = jdbcOperations;
    }

	public List<User> getUserList() {
        return jdbcOperations.query("SELECT * FROM testtable ;", new UserRowMapper());
    }

    public void addUser(User user) {
        jdbcOperations.update(
                "INSERT INTO testtable (text1, text2, text3) VALUES (?,?,?);",
                user.getText1(),
                user.getText2(),
                user.getText3()
        );
    }

    public void updateUser(User user) {

    }

    public void deleteAllUsers() {
		jdbcOperations.update("DELETE FROM testtable;");
    }

    public static final class UserRowMapper implements RowMapper<User> {
        public User mapRow(ResultSet rs, int rowNum) throws SQLException{
            User userFromDB = new User(
                    rs.getString("text1"),
                    rs.getString("text2"),
                    rs.getString("text3")
            );

            userFromDB.setId(rs.getLong("id"));
            return userFromDB;
        }
    }

}
