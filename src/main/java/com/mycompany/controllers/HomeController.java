package com.mycompany.controllers;

import com.mycompany.dao.UserDao;
import com.mycompany.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.util.*;
import java.io.PrintWriter;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.nio.charset.Charset;

import org.apache.poi.util.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.*;

import java.util.regex.Pattern;
import java.util.regex.Matcher;


@Controller
public class HomeController {

    @Autowired
    UserDao userDao;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(ModelMap model){
		model.addAttribute("rowList", userDao.getUserList());
		return "home";
    }
	@RequestMapping(value = "/clear", method = RequestMethod.GET)
	public String clearTable(){
		userDao.deleteAllUsers();
		return "redirect:/";
	}


	@RequestMapping(value = "/ajaxpostadd", method = RequestMethod.POST)
	public String ajaxPostAdd(@RequestBody String data, ModelMap model){
		User b = new User("", data, "");
		userDao.addUser(b);
		model.addAttribute("rowList", userDao.getUserList());
		return "redirect:/";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addCoding(
		@RequestParam("text1") String t1,
		@RequestParam("text2") String t2,
		@RequestParam("text3") String t3,
		ModelMap model
	) throws Exception{
		//User b = new User(y(t1), y(t2), y(t3));
		User b = new User(t1, t2, t3);
		userDao.addUser(b);
		model.addAttribute("rowList", userDao.getUserList());
		return "redirect:/";
	}

	@RequestMapping(value = "/getxls", method = RequestMethod.GET )
	public void export1( HttpServletResponse response ) throws IOException {
		List<User> users = userDao.getUserList();
		
		Workbook wb = new HSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		Sheet sheet = wb.createSheet("new sheet");

		// creating headers for data columns
		Row row = sheet.createRow(0);
		row.createCell(0).setCellValue("Referent");
		row.createCell(1).setCellValue("MeaningVal");
		row.createCell(2).setCellValue("Comment");
		
		int rowIndex = 1;

		for(User user : users){
			row = sheet.createRow(rowIndex);
			row.createCell(0).setCellValue(createHelper.createRichTextString(user.getText1()));
			row.createCell(1).setCellValue(createHelper.createRichTextString(user.getText2()));
			row.createCell(2).setCellValue(user.getText3());

			rowIndex++;
		} 

		response.setHeader("Content-Disposition","attachment; filename=data.xls");

		ServletOutputStream out = response.getOutputStream();
		wb.write(out);
		out.flush();
		out.close();
	}

	public static String y(String txt){
/*
		String str = "";
		String[] arr = txt.replace("&#","").split(";");
		for (int i=0;i<arr.length;i++)
			str += String.valueOf(Character.toChars(Integer.parseInt(arr[i], 10)));
		return str;
*/


		String formatedString = "";
		char[] charArr = txt.toCharArray();
		for (int i=0; i<charArr.length; i++){
			if(charArr[i] != '&')
				formatedString += charArr[i];
			else {
				if (i+5 <= charArr.length && charArr[i+1] == '#'){
					String temp = "";
					for (int j=i+2;j<=i+5;j++)
						temp += charArr[j];
					if (temp.matches("-?\\d+(\\.\\d+)?")){
						formatedString += String.valueOf(Character.toChars(Integer.parseInt(temp, 10)));
						i=i+6;
					}
				} else
					formatedString += charArr[i];
			}
		}
		return formatedString;
	}
}
